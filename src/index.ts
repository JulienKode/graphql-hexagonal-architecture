import * as express from 'express';
import * as graphqlHTTP from 'express-graphql';
import { buildSchema } from 'graphql';

// Use cases
import { FetchUserUseCase } from './usercontext/usecases/fetchUser';

// Primary
import { UserQuery } from './usercontext/adapters/primary/query'
import { UserResolver } from './usercontext/adapters/primary/resolver'
import { UserType } from './usercontext/adapters/primary/type';

// Secondary
import { InMemoryUsers } from './usercontext/adapters/secondary/InMemory';

// Construct a schema, using GraphQL schema language
var schema = buildSchema(UserQuery + UserType);

// The root provides a resolver function for each API endpoint
var root = {
  ...UserResolver,
};

var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
  context: {
    user: {
      fetchUser: new FetchUserUseCase(new InMemoryUsers())
    },
  }
}));
app.listen(4000);

console.log('Running a GraphQL API server at localhost:4000/graphql');