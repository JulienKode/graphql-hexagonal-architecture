import { FetchUserUseCase } from "./usercontext/usecases/fetchUser";

export interface Context {
    user: {
        fetchUser: FetchUserUseCase
    }
}