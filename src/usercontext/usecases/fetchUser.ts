import { IUser } from '../domain/entities/IUser'
import { UsersGateway } from '../domain/gateways/UserGateway'

export class FetchUserUseCase {

    userGateway: UsersGateway

    constructor(userGateway: UsersGateway) {
        this.userGateway = userGateway;
    }

    retrieveAll(): Promise<IUser[]> {
        return this.userGateway.all();
    }

    retrieve(id: string): Promise<IUser> {
        return this.userGateway.get(id);
    }
}