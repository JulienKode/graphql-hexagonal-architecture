export const UserQuery = `
type Query {
  users: [User]
  get(id: String): User
},
`;