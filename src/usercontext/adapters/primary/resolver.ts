import { InMemoryUsers } from '../secondary/InMemory'
import { Context } from '../../../context';

export const UserResolver = {
    users(args, ctx: Context, info) {
        return ctx.user.fetchUser.retrieveAll();
      },

      get(parent, args, ctx: Context, info) {
        return ctx.user.fetchUser.retrieve(args.id);
      },
}