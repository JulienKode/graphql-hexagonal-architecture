export const UserType = `
type User {
    id: ID!
    email: String!
    firstname: String!
    lastname: String!
  },
`;