import { UsersGateway } from '../../domain/gateways/UserGateway'
import { IUser } from '../../domain/entities/IUser'
import { User } from '../../domain/entities/User'

export class InMemoryUsers implements UsersGateway {

    all(): Promise<IUser[]> {
        return new Promise<IUser[]>((resolve, reject) => { 
            resolve([new User('1', 'test@test.com', 'John', 'Doe'), new User('2', 'test2@test.com', 'John2', 'Doe2')]); 
        });
    }
    
    get(id: string): Promise<IUser> {
        return new Promise<IUser>((resolve, reject) => { 
            resolve(new User('2', 'test2@test.com', 'John2', 'Doe2')); 
        });
    }

}
