// import { IMessage } from './IMessage';

export interface IUser {

    id: string;

    email: string;

    password?: string;
    
    firstname: string;
    
    lastname: string;

    login(): Promise<string>

}
