// import { Observable, of } from 'rxjs'
import { IUser } from '../entities/IUser'

export interface UsersGateway {

    all(): Promise<IUser[]>
    
    get(id: string): Promise<IUser>

}
